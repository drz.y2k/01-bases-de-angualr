import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { HeroesModule } from './heroes/heroes.module';
import { ContadorModule } from './contador/contador.module';
import { DbzModule } from './dbz/dbz.module';

@NgModule({
  //componetes que se usan xd
  //es importante declarar solo componentes principales, es decir
  //los que es necesario cargar almomemnto de iniciar la aplicacion
  declarations: [
    AppComponent,
    
  ],
  // aqui normalmente van otros modulos
  imports: [
    BrowserModule,
    //importamos el modulo heroes para poder acceder a la vista de listado heroes
    HeroesModule,
    ContadorModule,
    DbzModule
  ],
  providers: [],
  //este modulo bootstrap aparentemente solo se crea qui
  bootstrap: [AppComponent]
})
export class AppModule { }
