import { Component, Input } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-personajese',
  templateUrl: './personajese.component.html'
})
export class PersonajeseComponent {
  //entre comillas del input ponemos otro nombre si es que queremos conocer esa propiedad de otra forma
  //@Input('data') personajes: Personaje[] = [];
  get personajes() {
    //aqui personajes es nuestro get, no directamente neustras variables
    return this.dbzService.personajes;
  }

  constructor(private dbzService: DbzService) {

  }

}
