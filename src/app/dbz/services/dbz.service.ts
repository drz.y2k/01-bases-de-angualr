import { Injectable } from "@angular/core";
import { Personaje } from '../interfaces/dbz.interface';

// el decordador de uns servicio es de tipo injectable
@Injectable()
export class DbzService {
    // de preferencia usar propiedades privadas
    private _personajes: Personaje[] = [{
        nombre: 'goku',
        poder: 15000,
    }, {
        nombre: 'vegetax',
        poder: 8500,
    }];

    get personajes(): Personaje[] {
        //usando spread operator rompemos la referencia de personajes privado para que si editan el get no afecte nuestra variable principal
        return [...this._personajes];
    }

    constructor() {
        console.log('servicio inicializad');
    }
    agregarPersonaje(personaje: Personaje) {
        this._personajes.push(personaje);
    }
}