import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent {


  @Input() nuevo: Personaje = {
    nombre: '',
    poder: 0
  }
  //se debe especificar el tipo de dato que se va a enviar en el event emitter,sino sabes que es aunque seas ponle any en el peor de los casos
  // @Output() onNuevoPersonaje: EventEmitter<Personaje> = new EventEmitter();

  // sino fuera por angular tendriamos que hacer asi el prevent defualt
  // agregar(event: any) {
  //   event.preventDefault();
  //   console.log('das');
  // }


  constructor(private dbzSvc: DbzService) {

  }

  agregar() {
    if (this.nuevo.nombre.trim().length === 0) {
      return;
    }
    this.dbzSvc.agregarPersonaje(this.nuevo);
    this.nuevo = { nombre: "", poder: 0 };
  }

  // agregar() {
  //   if (this.nuevo.nombre.trim().length === 0) {
  //     return;
  //   }

  //   console.log(this.nuevo);

  //   this.onNuevoPersonaje.emit(this.nuevo);
  //   this.nuevo = { nombre: "", poder: 0 };

  // }

}
