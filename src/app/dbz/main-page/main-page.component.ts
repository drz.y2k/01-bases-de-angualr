import { Component } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

//al no hacer exportacion de interface solo vaa funcionar en este archivo


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  //no se ocupa este este css jaja,
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent {

  // personajes:Personaje[]=[];

  nuevo: Personaje = {
    nombre: 'maestro roshi',
    poder: 1000
  }

  //mediante get se puede obtener informacion
  // get personajes(): Personaje[] {
  //   return this.dbzSvc.personajes;
  // }


  // cambiarNombre(event:any){
  //   console.log(event.target.value);
  // }
  //inyeccion de dependencias
  // constructor(private dbzSvc: DbzService) {
  //   // this.personajes=dbzSvc.personajes;
  // }


  constructor() {
    
     }

}
