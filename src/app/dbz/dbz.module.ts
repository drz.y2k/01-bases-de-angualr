//buena practica primero hacer importaciones de angular, luego de terceros, luego las que nosotros creamos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MainPageComponent } from './main-page/main-page.component';
import { PersonajeseComponent } from './personajese/personajese.component';
import { AgregarComponent } from './agregar/agregar.component';
import { DbzService } from './services/dbz.service';

@NgModule({
  declarations: [
    MainPageComponent,
    PersonajeseComponent,
    AgregarComponent
  ],
  exports: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  //son todos los servicios,seran como una instancia que se va  atener a lo largo de todo el modulo, en eset caso del modulo dbz
  providers: [
    DbzService
  ]
})
export class DbzModule { }
