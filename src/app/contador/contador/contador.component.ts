// la clase export es para usarlo en otros lugares fuera de este archivo
import { Component } from '@angular/core';
@Component({
    selector: 'app-contador',
    template: `
            <h1>{{titulo}}</h1>
        <!-- <h1>{{1+1}}</h1> -->
        <!-- <button (click)="numero=numero+1;">+1</button> -->
        <!-- <button (click)="sumar()">+1</button> -->
        <h2>La base es {{baseNumer}}
        </h2>
        <button (click)="acumular(baseNumer)">+{{baseNumer}}</button>
        <span>{{numero}}</span>
        <button (click)="acumular(-baseNumer)">-{{baseNumer}}</button>
    `
})
export class ContadorComponent {
    public titulo: string = 'Contador APP';
    numero: number = 0;
    baseNumer: number = 5;

    // sumar() {
    //   this.numero+=1;
    // }
    // restar() {
    //   this.numero-=1;
    // }
    acumular(valor: number) {
        this.numero += valor;
    }
}