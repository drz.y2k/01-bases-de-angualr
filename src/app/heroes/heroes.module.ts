import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HeroeComponent } from './heroe/heroe.component';
import { ListadoComponent } from './listado/listado.component';

@NgModule({
    //en declaraciones va lo que contiene el modulo, ya sean componentes o pipes
    declarations: [
        HeroeComponent,
        ListadoComponent
    ],
    //en las exportaciones van las cosas que queremos que sean visibles fuera del modulo, como en este caso el componente de listado jiji
    exports: [
        ListadoComponent
    ],
    //usualmente solo se colocan modulos dentro de los imports
    imports: [
        //ocupamos el common module porque nos da la funcionalidad de las directivas ngif y ngfor por ejmplo
        CommonModule
    ]
})
export class HeroesModule {

}