import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {
  heroes: string[] = ['spiderman', 'hulk', 'flores','thor','captain america'];
  heroeBorradoF!:string;

  public borrarHeroe():void{
    console.log('borrando...');
    //shift borra el primer valor del arreglo
    this.heroeBorradoF=this.heroes.shift()||'';
    
    //this.heroes.splice(2,1);
  }
}
